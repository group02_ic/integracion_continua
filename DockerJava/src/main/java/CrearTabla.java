import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import javax.swing.JOptionPane;

@WebServlet("/CrearTabla")
public class CrearTabla extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 680453258361206091L;
	/**
	* Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
	* @param request servlet request
	* @param response servlet response
	*/
	static Connection conexion=null;
	static Statement sentencia=null;
	static ResultSet rs=null;
	
	public CrearTabla() {
		super();
	}
	
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException {
		/*response.getWriter().append("Serverd at: ").append(request.getContextPath());
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/index.jsp");
		dispatcher.forward(request, response);*/
		
		/*try {
			Class.forName("org.postgresql.Driver");
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "No se pudo cargar el puente JDBC-ODBC.");
			request.setAttribute("mensaje", "<br><p>ERROR AL EJECUTAR Class.forName(\"org.postgresql.Driver\");</p>");
			return;
		}
		try { 
			conexion = DriverManager.getConnection("jdbc:postgresql://180.10.0.2:5432/docpostgres", "postgres", "mysecretpassword");
		} 
		catch (Exception er) {
			request.setAttribute("mensaje", "<br><p>ERROR AL OBTENER LA CONEXION: " + "catch (Exception er)" + er.toString() + "</p>");
			System.out.println("catch (Exception er)" + er.toString());
			
		}
		
		try {
			String consulta="SELECT table_name FROM information_schema.columns WHERE table_name='tabla_docpg' AND table_catalog = 'docpostgres'";
			sentencia = conexion.createStatement();
			rs = sentencia.executeQuery(consulta);
			
			if (rs.getFetchSize()>0) {
				System.out.println("La tabla *tabla_docpg* ya existe. Puede obtener o ingresar datos a esta.");
				request.setAttribute("mensaje", "<br><p>La tabla *tabla_docpg* ya existe. Puede obtener o ingresar datos a esta.</p>");
			}
			else {
				sentencia.close();
				sentencia = conexion.createStatement();
				consulta="CREATE TABLE tabla_docpg (datos varchar(255))";
				sentencia.execute(consulta);
				System.out.println("sentencia.execute(CREATE TABLE tabla_docpg (datos varchar(255)));");

				request.setAttribute("mensaje", "<br><p>SE HA CREADO la tabla *tabla_docpg* SATISFACTORIAMENTE</p>");				
			}
			sentencia.close();
			conexion.close();
			//out.println("<a href=Nuevo.jsp>Agregar Nuevo</a>");
		} catch(Exception er){
			//JOptionPane.showMessageDialog(null,"Error de conexion" );
		}
		
		request.getRequestDispatcher("/creartabla.jsp").forward(request, response);*/
	}
	


	
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException {
		//processRequest(request, response);
		
		String mensaje="";
		
		try {
			Class.forName("org.postgresql.Driver");
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "No se pudo cargar el puente JDBC-ODBC.");
			mensaje = "ERROR AL EJECUTAR Class.forName(\\\"org.postgresql.Driver\\\")";
			request.setAttribute("mensaje", mensaje);
			System.out.println(request.getAttribute("mensaje"));
			return;
		}
		try { 
			conexion = DriverManager.getConnection("jdbc:postgresql://180.10.0.2:5432/docpostgres", "postgres", "mysecretpassword");
		} 
		catch (Exception er) {
			mensaje = "ERROR AL OBTENER LA CONEXION: " + "catch (Exception er)" + er.toString();
			request.setAttribute("mensaje", mensaje);
			System.out.println(request.getAttribute("mensaje"));
			System.out.println("catch (Exception er)" + er.toString());
			
		}
		System.out.println("antes del try2");

		try {
			String consulta="SELECT table_name FROM information_schema.columns WHERE table_name='tabla_docpg' AND table_catalog = 'docpostgres'";
			sentencia = conexion.createStatement();
			rs = sentencia.executeQuery(consulta);
			
			if (rs.getFetchSize()>0) {
				System.out.println("La tabla *tabla_docpg* ya existe. Puede obtener o ingresar datos a esta.");
				mensaje = "La tabla *tabla_docpg* ya existe. Puede obtener o ingresar datos a esta.";
				request.setAttribute("mensaje", mensaje);
				System.out.println(request.getAttribute("mensaje"));
			}
			else {
				sentencia.close();
				sentencia = conexion.createStatement();
				consulta="CREATE TABLE tabla_docpg (datos varchar(255))";
				sentencia.execute(consulta);
				System.out.println("sentencia.execute(CREATE TABLE tabla_docpg (datos varchar(255)));");

				mensaje = "SE HA CREADO la tabla *tabla_docpg* SATISFACTORIAMENTE";
				request.setAttribute("mensaje", mensaje);				
				System.out.println(request.getAttribute("mensaje"));
			}
			sentencia.close();
			conexion.close();
			//out.println("<a href=Nuevo.jsp>Agregar Nuevo</a>");
		} catch(Exception er){
			//JOptionPane.showMessageDialog(null,"Error de conexion" );
		}
		
		request.getRequestDispatcher("/creartabla.jsp").forward(request, response);
		System.out.println("request.getRequestDispatcher(\"/creartabla.jsp\").forward(request, response);");
	}
	
	/**
	* Returns a short description of the servlet.
	*/
	public String getServletInfo() {
		System.out.println("public String getServletInfo() {;");
		return "public String getServletInfo() {";
	}

}